package xxx.sai7110.chess;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.*;
import android.view.MotionEvent;
import android.view.View;
import android.view.*;
import java.sql.*;


public class CanvasTest1View extends View {
	WindowManager wm = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
	Display disp = wm.getDefaultDisplay();
	
	int width = disp.getWidth();
	int height = disp.getHeight();
	private int   rectW = width - 10;
	private int   rectH = height - height/5;
	private Rect  rect;
	private Paint Wpaint;
	private Paint Bpaint;
  private Paint Tpaint;
	private Paint waku;
	private Paint paint;
	//int k[] = {4,3};
	int w = rectW/8, h = w;
  int hs = height/2 - rectW/2;//画面の高さ合わせ
	//int ww = w*4, hh = h*3;
  // 0:null,1:king,2:queen,3:bishop,4:knight,5:rook,6:paun
  int field[][] = {
      {11,10,9,8,7,9,10,11},
      {12,12,12,12,12,12,12,12},
      {0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0},
      {0,0,0,0,0,0,0,0},
      {6,6,6,6,6,6,6,6},
      {5,4,3,2,1,3,4,5}
  };
  int touch_x = 0;
  int touch_y = 0;
  private int touch_steate = 0;

	public CanvasTest1View(Context context) {
		super(context);
		rect = new Rect();
    
		Wpaint = new Paint();
		Wpaint.setColor(Color.WHITE);
		//Wpaint.setStyle(Paint.Style.STROKE);
		Wpaint.setStrokeJoin(Paint.Join.ROUND);
		Wpaint.setStrokeCap(Paint.Cap.ROUND);
		Wpaint.setStrokeWidth(10);
		
		Bpaint = new Paint();
		Bpaint.setColor(Color.BLACK);
		//Bpaint.setStyle(Paint.Style.STROKE);
		Bpaint.setStrokeJoin(Paint.Join.ROUND);
		Bpaint.setStrokeCap(Paint.Cap.ROUND);
		Bpaint.setStrokeWidth(10);
		
    Tpaint = new Paint();
    Tpaint.setColor(Color.RED);
    
		waku = new Paint();
		waku.setColor(Color.GRAY);
		waku.setStyle(Paint.Style.STROKE);
		
		paint = new Paint();
		paint.setColor(Color.BLUE);
		//paint.setStrokeCap(Paint.Cap.ROUND);
		//paint.setStrokeJoin(Paint.Join.ROUND);
		//paint.setStrokeWidth(10);
		paint.setTextSize(200);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		//steageCreate
    for(int x=0; x<8; x++){
			for(int y=0; y<8; y++){
				if( ((x+y) % 2) == 0){
					canvas.drawRect(w*x, h*y+hs, w*(x+1), h*(y+1)+hs, Wpaint);
				} else {
					canvas.drawRect(w*x, h*y+hs, w*(x+1), h*(y+1)+hs, Bpaint);
				}
        
			}
		}
    //onSelect
    if(touch_steate != 0){
        switch(touch_steate){
            case 1:
                lightKing(canvas);
                break;
            case 2:
                lightQueen(canvas);
                break;
            case 3:
                lightBishop(canvas);
                break;
            case 4:
                lightKnight(canvas);
                break;
            case 5:
                lightRook(canvas);
                break;
            case 6:
                lightPawn(canvas);
        }
    }
		canvas.drawRect(0,hs,rectW,h*8+hs,waku);
    
    for(int y=0; y<8; y++){
        for(int x=0; x<8; x++){
            canvas.drawText(""+field[y][x],w*x,h*(y+1)+hs,paint);
        }
    }
      canvas.drawText(""+touch_x,10,150,paint);
      canvas.drawText(""+touch_y,300,150,paint);
      canvas.drawText(""+touch_steate,600,150,paint);
      
	}
  ///////////////////////////////////
  //            Piece              //
  ///////////////////////////////////
  void lightKing(Canvas c){
      //c.drawRect(w*touch_x, h*touch_y-1+hs, w*(touch_x+1), h*(touch_y-1)+hs, Tpaint);
      lightDraw(c,-1,-1);
      lightDraw(c,-1,0);
      lightDraw(c,-1,+1);
      lightDraw(c,0,+1);
      lightDraw(c,0,-1);
      lightDraw(c,+1,-1);
      lightDraw(c,+1,0);
      lightDraw(c,+1,+1);
  }
  void lightQueen(Canvas c){
      
  }
  void lightBishop(Canvas c){
      
  }
  void lightKnight(Canvas c){
      lightDraw(c,-1,-2);
      lightDraw(c,-2,-1);
      lightDraw(c,+1,-2);
      lightDraw(c,+2,-1);
      lightDraw(c,-1,+2);
      lightDraw(c,-2,+1);
      lightDraw(c,+1,+2);
      lightDraw(c,+2,+1);
  }
  void lightRook(Canvas c){
      boolean top   = true;
      boolean right = true;
      boolean botom = true;
      boolean left  = true;
      for(int i=0; i<8; i++){
          if(top && field[touch_y-i][touch_x] != 0){
              lightDraw(c,0,touch_y+i);
          }
      }
  }
  void lightPawn(Canvas c){
      lightDraw(c,0,-1);
  }
  //////////////////////////////////
  
  void lightDraw(Canvas c, int x, int y){
      c.drawRect(w*(touch_x+x), h*(touch_y+y)+hs, w*((touch_x+x)+1), h*((touch_y+y)+1)+hs, Tpaint);
  }
    
  
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		switch(e.getAction()){
			case MotionEvent.ACTION_DOWN:
				//rect.set(10, 10, rectW, rectH);
				int get_x = (int)e.getX();
				int get_y = (int)e.getY();
        
        
        for(int y=0; y<8; y++){
            for(int x=0; x<8; x++){
                if(((x*w < get_x) && ((x+1)*w > get_x))
                    &&
                   (((y*h)+hs < get_y) && (h*(y+1)+hs > get_y))){
                    touch_x = x; touch_y = y;
                    if(field[y][x] != 0 && field[y][x] < 7){
                        
                        touch_steate = field[y][x];
                        //break;
                    } else {
                        touch_steate = 0;
                    }
                } else {
                    //touch_x = 0;
                    //touch_y = 0;
                    //touch_steate = 0;
                }
            }
        }
        
				invalidate();
				break;
		}

		return true;
	}

}
