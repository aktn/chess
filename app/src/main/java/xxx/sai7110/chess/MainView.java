package xxx.sai7110.chess;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.*;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.*;
import java.*;
import java.sql.*;
import java.security.*;
import java.util.*;
import java.util.concurrent.*;
import java.lang.*;
import android.os.*;
import android.gesture.*;
import java.lang.annotation.*;
import android.view.GestureDetector.*;
import android.view.LayoutInflater.*;


public class MainView extends SurfaceView implements GestureDetector.OnDoubleTapListener,GestureDetector.OnGestureListener, SurfaceHolder.Callback {
	private GestureDetector ges;
	SurfaceHolder holder = getHolder();
	WindowManager wm = (WindowManager)getContext().getSystemService(Context.WINDOW_SERVICE);
	Display disp = wm.getDefaultDisplay();

	int col = 6+2;
	int row = 14+1;
	int clickSteat = 0;
	int width = disp.getWidth();
	int height = disp.getHeight();
	private int   fieldW = width - width/3;
	private int   fieldH = height - height/5;
	private int startGame = 0;
	//private Rect  rect;
	private Paint startPaint;
	private Paint block = new Paint();
	private Paint cn = new Paint();
	private Paint cb = new Paint();
	private Paint cr = new Paint();
	private Paint cy = new Paint();
	private Paint cg = new Paint();
	private Paint cp = new Paint();
	private Paint p;
	public int count= 100;
	private int timerCheck = 0;
	private int steatX = 3;
	private int steatY = 1;
	private int steatX2 = 4;
	private int steatY2 = 1;
	long seed = Runtime.getRuntime().freeMemory() + System.currentTimeMillis();
	
	Random rand = new Random(seed);
	int steatC = rand.nextInt(5)+1;
	int nextC = rand.nextInt(5)+1;
	private int nextC2 = rand.nextInt(5)+1;
	int k[] = {4,3};
	int field[][] = new int[col+1][row+1];
	int gridW = fieldW/col, gridH = fieldH/row;
	int ww = gridW, hh = gridH;
	
	public MainView(Context context) {
		super(context);
		setFocusable(true);
        getHolder().addCallback(this);
		//setOnTouchListener(this);
		ges = new GestureDetector(context,this);
		setClickable(true);
		setFocusable(true);
		//rect = new Rect();
	}
	
	
	@Override
    public void surfaceCreated(SurfaceHolder holder) {
        // SurfaceViewが作成された時の処理（初期画面の描画等）を記述
		
		doDraw(holder);
    }
	
	@Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // SurfaceViewが変化（画面の大きさ，ピクセルフォーマット）した時のイベントの処理を記述
    }
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
        // SurfaceViewが廃棄されたる時の処理を記述
    }
	
	private void doDraw(SurfaceHolder holder) {
        Canvas canvas = holder.lockCanvas();
        onDraw(canvas);
		//tett(canvas);
        holder.unlockCanvasAndPost(canvas);
    }
	
	@Override
	protected void onDraw(Canvas canvas) {
		

		startPaint = new Paint();
		startPaint.setColor(Color.BLUE);
		//paint.setStrokeCap(Paint.Cap.ROUND);
		//paint.setStrokeJoin(Paint.Join.ROUND);
		//paint.setStrokeWidth(10);
		startPaint.setTextSize(100);
		
		for(int x=0; x<col; x++){
			for(int y=0; y<row; y++){
				if(y == 14){
					field[x][y] = -1;
				} else if(x == 0){
					field[x][y] = -1;
				} else if(x == 7){
					field[x][y] = -1;
				} else {
					field[x][y] = 0;
				}
			}
		}
		
		
		canvas.drawText("Tap on START",width/5,height/2,startPaint);
	}
	
	
	public void puyoDraw(Canvas canvas,int x,int y){
		int color = field[x][y];
		
		block.setColor(Color.WHITE);
		cn.setColor(Color.BLACK);
		cb.setColor(Color.BLUE);
		cr.setColor(Color.RED);
		cg.setColor(Color.GREEN);
		cy.setColor(Color.YELLOW);
		cp.setColor(Color.CYAN);
		
		//canvas.drawText("sss",299,310,t);
		if(color == 0){
			canvas.drawRect(gridW*(x), gridH*(y-1), gridW*(x+1), gridH*y, cn);
		} else if(color == -1){
			canvas.drawRect(gridW*(x), gridH*(y-1), gridW*(x+1), gridH*y, block);
		} else if(color == 1){
			canvas.drawOval(new RectF(gridW*(x), gridH*(y-1), gridW*(x+1), gridH*y), cr);
		} else if(color == 2){
			canvas.drawOval(new RectF(gridW*(x), gridH*(y-1), gridW*(x+1), gridH*y), cb);
		} else if(color == 3){
			canvas.drawOval(new RectF(gridW*(x), gridH*(y-1), gridW*(x+1), gridH*y), cg);
		} else if(color == 4){
			canvas.drawOval(new RectF(gridW*(x), gridH*(y-1), gridW*(x+1), gridH*y), cy);
		} else if(color == 5){
			canvas.drawOval(new RectF(gridW*(x), gridH*(y-1), gridW*(x+1), gridH*y), cp);
		}
	}
	
	
	public void update(SurfaceHolder holder){
		Canvas canvas = holder.lockCanvas();
		p = new Paint();
		p.setColor(Color.RED);
		p.setTextSize(200);
		
		//canvas.drawRect(fieldW,0,width,fieldH/2,p);
		for(int x=0; x<col; x++){
			for(int y=0; y<row; y++){
				puyoDraw(canvas,x,y);
			}
		}
		if(nextC == 1){
			canvas.drawOval(new RectF(fieldW,0,width,fieldH/4),cr);
		} else if(nextC == 2){
			canvas.drawOval(new RectF(fieldW,0,width,fieldH/4),cb);
		} else if(nextC == 3){
			canvas.drawOval(new RectF(fieldW,0,width,fieldH/4),cg);
		} else if(nextC == 4){
			canvas.drawOval(new RectF(fieldW,0,width,fieldH/4),cy);
		} else if(nextC == 5){
			canvas.drawOval(new RectF(fieldW,0,width,fieldH/4),cp);
		}
		
		if(nextC2 == 1){
			canvas.drawOval(new RectF(fieldW,fieldH/4,width,fieldH),cr);
		} else if(nextC2 == 2){
			canvas.drawOval(new RectF(fieldW,fieldH/4,width,fieldH),cb);
		} else if(nextC2 == 3){
			canvas.drawOval(new RectF(fieldW,fieldH/4,width,fieldH),cg);
		} else if(nextC2 == 4){
			canvas.drawOval(new RectF(fieldW,fieldH/4,width,fieldH),cy);
		} else if(nextC2 == 5){
			canvas.drawOval(new RectF(fieldW,fieldH/4,width,fieldH),cp);
		}
		
		holder.unlockCanvasAndPost(canvas);
	}
	
	public void Check(){
		int c = field[steatX][steatY];
		
	}
	
	TimerTask task = new TimerTask(){
		@Override
		public void run(){
			timerCheck = 1;
			SurfaceHolder hol = getHolder();
			if(steatY == row || field[steatX][steatY] != 0 || steatY2 == row || field[steatX2][steatY2] != 0){
				steatX = 3;
				steatY = 1;
				steatX2 = 4;
				steatY2 = 1;
				steatC = nextC;
				nextC = nextC2;
				nextC2 = rand.nextInt(5)+1;				
			} else {
				field[steatX][steatY] = steatC;
				field[steatX][steatY-1] = 0;
				field[steatX2][steatY2] = steatC;
				field[steatX2][steatY2-1] = 0;
				update(hol);
				steatY = steatY + 1;
				steatY2 = steatY2 +1;
			}
			timerCheck = 0;
		}
	};
	
	////////////////////////
	//         UI         //
	////////////////////////
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		ges.onTouchEvent(event);
		
		return true;
	}
	
	@Override
	public boolean onDown(MotionEvent e){
		
		if(startGame == 0){
			Timer timer = new Timer();
			timer.schedule(task,1000,500);
			
			startGame = 1;
		}
		//update(hol);
		invalidate();
		
		return true;
		
	}
	
	@Override
	public boolean onFling(MotionEvent el,MotionEvent e2,float veloctyX,float veloctyY){
		if(timerCheck != 1){
		if(veloctyX > 0){
			/*if((field[steatX+1][steatY-1] == 0)
			|| ((steatX+1 == steatX2 && steatY == steatY2)
			 && (field[steatX2+1][steatY2-1] == 0))
			|| ((steatX2+1 == steatX && steatY2-1 == steatY)
			 && (field[steatX+1][steatY-1] == 0))
			){*/
				field[steatX+1][steatY-1] = steatC;
				field[steatX][steatY-1] = 0;
				
				field[steatX2+1][steatY2-1] = steatC;
				field[steatX2][steatY2-1] = 0;
				
				steatX = steatX+1;
				steatX2 = steatX2+1;
			
			//steatC = 1;
			//}
		} else {
			if((field[steatX-1][steatY-1] == 0)
			|| ((steatX-1 == steatX2 && steatY-1 == steatY2)
			 && (field[steatX2-1][steatY2-1] == 0))
			|| ((steatX2-1 == steatX && steatY2-1 == steatY)
			 && (field[steatX-1][steatY-1] == 0))
			){
				field[steatX-1][steatY-1] = steatC;
				field[steatX][steatY-1] = 0;
				steatX = steatX-1;
				
				field[steatX2-1][steatY2-1] = steatC;
				field[steatX2][steatY2-1] = 0;
				steatX2 = steatX2-1;
			}
		}
		update(holder);
		}
		return true;
	}
	
	@Override
	public void onLongPress(MotionEvent e){
		
	}
	
	@Override
	public boolean onScroll(MotionEvent el,MotionEvent e2,float distanceX,float distanceY){
		return true;
	}
	
	@Override
	public void onShowPress(MotionEvent e){
		
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent p1){
		// TODO: Implement this method
		return true;
	}
	
	@Override
	public boolean onSingleTapUp(MotionEvent e){
		return true;
	}

	@Override
	public boolean onDoubleTap(MotionEvent p1){
		// TODO: Implement this method
		
		//field[4][4] = -1;
		//update(holder);
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent p1){
		// TODO: Implement this method
		//field[4][5] = -1;
		//update(holder);
		
		return true;
	}
	
}
