package xxx.sai7110.chess;

import android.app.Activity;
import android.os.*;

public class MainActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(new CanvasTest1View(this));
	}
	
}
